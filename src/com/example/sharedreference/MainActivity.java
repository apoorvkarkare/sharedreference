package com.example.sharedreference;

import android.app.Activity;
import android.app.AlertDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	SharedPreferences sharedpreferences;
	TextView textview;
	EditText edittext;
	Button setButton;
	String text;
	int count;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textview = (TextView) findViewById(R.id.text_view);
		edittext = (EditText) findViewById(R.id.edit_text);
		setButton = (Button) findViewById(R.id.set_button);
		
		sharedpreferences = getSharedPreferences("my", Context.MODE_PRIVATE);
		//textview.setText(sharedpreferences.getString("text", ""));
		
		if (sharedpreferences.getString("text", null) != null
				) {
			textview.setText(sharedpreferences.getString("text", ""));
			System.out.println(sharedpreferences.getString("text", ""));
		}
		setButton.setOnClickListener(new OnClickListener() {

			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stu

				System.out.println("first:"
						+ sharedpreferences.getString("text", null));
				if (sharedpreferences.getString("text", null) != null) {
					System.out.println("i m in" + text);
					count++;
					textview.setText(sharedpreferences.getString("text", null));
					if (count > 0) {
						
						pressed();
					
					}
					/*Editor editor = sharedpreferences.edit();
					editor.putString("text", text);
					editor.commit();*/
				} else {
					count = 0;
					text = edittext.getText().toString();
					textview.setText(text);
					Editor editor = sharedpreferences.edit();
					editor.putString("text", text);
					editor.putLong("count", ++count);
					editor.commit();
					System.out.println("shared:"
							+ sharedpreferences.getString("text", null));
					// count++;
				}
			}
		});

	}
	

	public void pressed() {
	    // Auto-generated method stub
	    new AlertDialog.Builder(this)
	    .setIcon(android.R.drawable.ic_dialog_alert)
	    .setTitle("DIALOG    ")//R.
	    .setMessage("Do you want to save this preference?")
	    .setPositiveButton("YES", new DialogInterface.OnClickListener() {

	        @Override
	        public void onClick(DialogInterface dialog, int which) {

	            //Stop the activity
	            //YourClass.this.finish();
	        	text = edittext.getText().toString(); 
	        	//textview.setText(sharedpreferences.getString("text", null));
				Editor editor = sharedpreferences.edit();
				editor.putString("text", text);
				editor.commit();
	            
	            Toast.makeText(getApplicationContext(),"HELLO Your Prefrence is saved for next time" ,Toast.LENGTH_SHORT).show();
	            MainActivity.this.finish();
	        }

	    })
	    .setNegativeButton("NO", null)
	    .show();

	    //super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
